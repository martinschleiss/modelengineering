package at.ac.tuwien.big.views.gen

import java.io.File
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.ac.tuwien.big.views.ViewModel
import at.ac.tuwien.big.views.ViewGroup
import at.ac.tuwien.big.views.View
import at.ac.tuwien.big.views.ClassIndexView
import at.ac.tuwien.big.views.ReadView
import at.ac.tuwien.big.views.DeleteView
import at.ac.tuwien.big.views.Property
import at.ac.tuwien.big.views.Class
import at.ac.tuwien.big.views.ElementGroup
import at.ac.tuwien.big.views.ClassOperationView
import at.ac.tuwien.big.views.ViewElement
import at.ac.tuwien.big.views.PropertyElement
import at.ac.tuwien.big.views.UpdateView
import at.ac.tuwien.big.views.CreateView
import javax.swing.LayoutStyle
import at.ac.tuwien.big.views.Link
import at.ac.tuwien.big.views.AssociationElement
import at.ac.tuwien.big.views.Text
import at.ac.tuwien.big.views.DateTimePicker
import at.ac.tuwien.big.views.Selection
import at.ac.tuwien.big.views.EnumerationLiteralItem
import at.ac.tuwien.big.views.SelectionItem
import org.eclipse.ocl.pivot.ids.EnumerationLiteralId
import at.ac.tuwien.big.views.List
import at.ac.tuwien.big.views.Table
import at.ac.tuwien.big.views.Column
import at.ac.tuwien.big.views.Condition
import at.ac.tuwien.big.views.CompositeCondition
import at.ac.tuwien.big.views.ComparisonCondition

class View2HTMLGenerator implements IGenerator {
	
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		if (resource.contents.get(0) instanceof ViewModel) {
			var viewModel = resource.contents.get(0) as ViewModel		
			val htmlFileName = new File(getWelcomeGroup(viewModel).toLowerCase+".html");
			fsa.generateFile(
				htmlFileName.toString,'''
					<!DOCTYPE html>
					«var welcomegroup_name = getWelcomeGroup(viewModel).name»
					<html lang="en" data-ng-app="«welcomegroup_name»App">
					«generateHead(viewModel)»
					<body data-ng-controller="«welcomegroup_name»Controller">

«««					//add HTML Elements here
					«generateNavigationBar(viewModel)»
					«FOR ViewGroup viewGroup: viewModel.viewGroups»
						«generateCreateAndUpdateViews(viewGroup, viewModel)»
						«generateClassIndexViews(viewGroup)»
						«generateReadAndDeleteViews(viewGroup)»
					«ENDFOR»
					</body>
					</html>'''	
			)	
		}
	}
	
	def generateHead(ViewModel viewModel) { '''
		<head>
			<title>Views</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<script src="../assets/moment-with-locales.js"></script>
			<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
			<script src="../assets/datetimepickerDirective.js"></script>
			<script src="../assets/views.js"></script>
			<script src="«getWelcomeGroup(viewModel).name».js"></script>
			<script type="text/javascript">
					$(document).ready(				 
						function(){				 
		view.addWelcomePage('«viewModel.viewGroups.filter[v | v.isWelcomeViewGroup].get(0).views.filter[v | v.isStartView].get(0).name.replaceAll("\\W", "")»');
						  	view.init();
					});
			</script>
		</head>'''
	}
	
	def generateNavigationBar(ViewModel viewModel) {
		'''
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div>
						<ul class="nav navbar-nav">
						«FOR ViewGroup viewGroup : viewModel.viewGroups»
							<li><a href="" class="viewgroup" target="«viewGroup.views.filter[v | v.isStartView].get(0).name.replaceAll("\\W", "")»">«viewGroup.name»</a></li>
						«ENDFOR»
						</ul>
					</div>
				</div>
			</nav>
		'''
	}
	
	def generateClassIndexViews(ViewGroup viewGroup) {
		
		'''
		«FOR View view : viewGroup.views»
			«IF view instanceof ClassIndexView»
				«var cv = view as ClassIndexView»
				<div class="container" id="«view.name.replaceAll("\\W", "")»">
					<h2>«view.header»</h2>
					<h3>«view.description»</h3>
				    <ul>
				 		<li data-ng-repeat="«view.class_.name.toLowerCase» in «view.class_.name.toLowerCase»s">
				 			{{ «view.class_.name.toLowerCase».«view.class_.id.name» }}
							«FOR Link l : view.link»
								«generateLink(l)»
							«ENDFOR»
				 		</li>
				 		«FOR Link l : cv.link»
				 			«generateAddButton(l)»
				 		«ENDFOR»
					</ul>
				</div>
			«ENDIF»
		«ENDFOR»
		'''
		
	}
	
	def generateReadAndDeleteViews(ViewGroup viewGroup) {
		
		'''
		«FOR View view : viewGroup.views»
			«IF view instanceof ReadView || view instanceof DeleteView»
			«var operationView = view as ClassOperationView»
			<div class="modal fade" id="modal«view.name.replaceAll("\\W", "")»">
				<div class="modal-dialog">
			    	<div class="modal-content">
			        	<div class="modal-header">
							<h4 class="modal-title">«view.header»</h4>
						</div>
			        	<div class="modal-body">
							<p>«view.description»</p>
							<h5>«view.name»</h5>
				       		«FOR ElementGroup p: operationView.elementGroups»
				       			«FOR ViewElement ve: p.viewElements»
				       				«IF ve instanceof PropertyElement»
				    					«var pe = ve as PropertyElement»
										<p>«ve.label»: {{ «view.class_.name.toLowerCase».«pe.property.name» }}</p>
				       				«ENDIF»
				       			«ENDFOR»
				       		«ENDFOR»
						</div>
						<div class="modal-footer">
							«generateModalButton(operationView)»
						</div>
					</div>
				</div>
			</div>
			«ENDIF»
		«ENDFOR»
		'''
		
	}
	
	def generateCreateAndUpdateViews(ViewGroup viewGroup, ViewModel model) {
		'''
		«FOR View view : viewGroup.views»
			«IF view instanceof CreateView || view instanceof UpdateView»
				<div class="container" id="«view.name.replaceAll("\\W", "")»"> 
					<h2>«view.header»</h2>
					<form name="«view.name.replaceAll("\\W", "")»Form" novalidate>
						<p>«view.description»</p>
						<div class="panel-group">
				      		«var operationView = view as ClassOperationView»
				      		«IF operationView.layout.alignment == at.ac.tuwien.big.views.LayoutStyle::HORIZONTAL»
					      		<div class="row">
					      			«generateElementGroups(operationView)»
					      		</div>
				      		«ELSE»
				      			«generateElementGroups(operationView)»
				      		«ENDIF»
				    	</div>
				      	«var cv = view as ClassOperationView»
				      	«generateSaveButton(cv, model)»
				  	</form>
				</div>
			«ENDIF»
		«ENDFOR»
		'''		
	}
	
	def generateElementGroups(ClassOperationView view) {
		'''
		«FOR ElementGroup e: view.elementGroups»

			<div class="elementgroup
				«IF view.layout.alignment == at.ac.tuwien.big.views.LayoutStyle::HORIZONTAL»
					 col-sm-6 
				«ENDIF»
			" «generateCondition(e.condition, view.class_)»>
				<h4>«e.header»</h4>
				<div class="panel-body">
					«IF view.layout.alignment == at.ac.tuwien.big.views.LayoutStyle::HORIZONTAL»
					<div class="form-inline" role="form"> 
«««					only for views with horizontal layout 
			    		«FOR ViewElement ve: e.viewElements»
			    			«generateViewElement(ve, view.class_, view)»
			    		«ENDFOR»
			    	</div>
			    	«ELSE»
			    		«FOR ViewElement ve: e.viewElements»
			    			«generateViewElement(ve, view.class_, view)»
			    		«ENDFOR»
			    	«ENDIF»
			  	</div>
			</div>
		«ENDFOR»
		'''
	}

	def generateViewElement(ViewElement ve, Class c, View v) {
		'''
		«IF ve instanceof PropertyElement»
			«var pe = ve as PropertyElement»
			«generatePropertyElement(pe, c, v)»
		«ELSEIF ve instanceof AssociationElement»
			«var ae = ve as AssociationElement»
			«generateAssociationElement(ae, c)»
		«ENDIF»
		'''
	}
	
	def generatePropertyElement(PropertyElement pe, Class c, View v) {
		'''
		«IF pe instanceof Text»
			«var t = pe as Text»
			«IF t.isLong»
				«generateLongTextElement(t, c, v)»
			«ELSE»
				«generateShortTextElement(t, c, v)»
			«ENDIF»		
		«ELSEIF pe instanceof DateTimePicker»
			«var dtp = pe as DateTimePicker»
			«generateDateTimePickerElement(dtp, c, v)»
		«ELSEIF pe instanceof Selection»
			«var s = pe as Selection»
			«generateSelectionElement(s, c, v)»
		«ENDIF»
		'''
	}
	//«_»
	
	def generateLongTextElement(Text t, Class c, View v) {
		'''
		<div class="form-group">
		<label for="«t.elementID.name»">«t.getLabel.name.toFirstUpper»
		«IF t.property.lowerBound == 1 && t.property.upperBound == 1»
		<span>*</span>
		«ENDIF»
		:</label>
		<textarea class="form-control" rows="4" id="«t.elementID.name»" name="«t.property.name.toLowerCase»"
		data-ng-model="new«c.name.toLowerCase».«t.property.name.toLowerCase»" 
		 «IF t.property.lowerBound == 1 && t.property.upperBound == 1»
		 required
		«ENDIF»  
		«IF t.format != null»
			data-ng-pattern="/«t.format»/"
		«ENDIF»
		«generateCondition(t.condition, c)» >
		</textarea>
		«val pe = t as PropertyElement»
		«generateErrorSpanTags(pe, v)»
		</div>
		'''
	}
	
	def generateShortTextElement(Text t, Class c, View v) {
		'''
		<div class="form-group">
		<label for="«t.getElementID.name»">«t.getLabel.name.toFirstUpper»
		«IF t.property.lowerBound == 1 && t.property.upperBound == 1»
		<span>*</span>
		«ENDIF»
				:</label>
		<input type="text" class="form-control" id="«t.elementID.getName»" name="«t.property.name.toLowerCase»"
		data-ng-model="new«c.name.toLowerCase».«t.property.name.toLowerCase»" 
		 «IF t.property.lowerBound == 1 && t.property.upperBound == 1»
		 required
		«ENDIF» 
		«IF t.format != null»
			data-ng-pattern="/«t.format»/"
		«ENDIF»
		«generateCondition(t.condition, c)» />
		«val pe = t as PropertyElement»
		«generateErrorSpanTags(pe, v)»
		</div>
		'''
	}
	
	def generateDateTimePickerElement(DateTimePicker dtp, Class c, View v) {
		'''
		<div class="form-group">
			<div class="row">
				<div class="col-xs-6 col-sm-12">
					<div class="form-group">
						<label for="«dtp.getElementID.name»">«dtp.getLabel.name.toFirstUpper»
						«IF dtp.property.lowerBound == 1 && dtp.property.upperBound == 1»
						<span>*</span>
						«ENDIF»
								:</label>
						<div class="input-group date" id="picker«dtp.getElementID.name»" style="«IF dtp.property.name.toUpperCase.equals("DATE")»calendar«ELSE»time«ENDIF»">
							<input type="text" class="form-control" id="«dtp.getElementID.name»" name="«dtp.getLabel.name»"
							data-ng-model="new«c.name.toLowerCase».«dtp.property.name.toLowerCase»" 
								«IF dtp.format != null»
									data-ng-pattern="/«dtp.format»/"
								«ENDIF»
							«generateCondition(dtp.condition, c)» />
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-«IF dtp.property.name.toUpperCase.equals("DATE")»calendar«ELSE»time«ENDIF»"></span>
							</span>
						</div>
		</div>
		</div>
		</div>
		«val pe = dtp as PropertyElement»
		«generateErrorSpanTags(pe, v)»
		</div>
		'''
	}
	
	def generateSelectionElement(Selection s, Class c, View v) {
		'''
		<div class="form-group">
		<label for="«s.getElementID.name»">«s.getLabel.name.toFirstUpper»
		«IF s.property.lowerBound == 1 && s.property.upperBound == 1»
		<span>*</span>
		«ENDIF»
				:</label>
		<select data-ng-option class="form-control" id="«s.elementID.name»"
		data-ng-model="new«c.name.toLowerCase».«s.property.name.toLowerCase»" 		 
		«IF s.property.lowerBound == 1 && s.property.upperBound == 1»
				 required
		«ENDIF» «generateCondition(s.condition, c)» >
		<option value="" disabled selected>Select your option</option>
		«FOR si : s.selectionItems»
			«IF si instanceof EnumerationLiteralItem»
			«var eli = si as EnumerationLiteralItem »
				«generateEnumerationLiteralItemElement(eli)»
			«ELSE»
				«generateSelectionItemElement(si)»
			«ENDIF»
		«ENDFOR»
		</select>
		«val pe = s as PropertyElement»
		«generateErrorSpanTags(pe, v)»
		</div>

		'''
	}
	
	def generateEnumerationLiteralItemElement(EnumerationLiteralItem eli) {
		'''
		<option value="«eli.enumerationLiteral.value»">«eli.enumerationLiteral.name»</option>
		'''
	}
	
	def generateSelectionItemElement(SelectionItem si) {
		'''
		<option value="«si.value»">«si.value»</option>
		'''
	}
		
	def generateAssociationElement(AssociationElement ae, Class c) {
		'''
		<div class="form-group">
			<div «generateCondition(ae.condition, c)»>
				<h5>«ae.label»</h5>
				«IF ae instanceof List»
					«var l = ae as List»
					«generateList(l, c)»
				«ELSEIF ae instanceof Table»
					«var t = ae as Table»
					«generateTable(t, c)»
				«ENDIF»
	 		«FOR Link l : ae.link»
	 			«generateAddButton(l)»
	 		«ENDFOR»
			</div>
		</div>
		'''
	}
	
	def generateList(List l, Class c) {
		'''
		<ul id=”«l.elementID»”>
		<li data-ng-repeat="«l.association.navigableEnd.type.name.toLowerCase» in «l.association.navigableEnd.type.name.toLowerCase»s">
		«var idName = l.association.navigableEnd.type as Class»
		{{ «l.association.navigableEnd.type.name.toLowerCase».«idName.id.name»}}
 		«FOR Link li : l.link»
 			«generateLink(li)»
 		«ENDFOR»
		</li>
		</ul>
		'''
	}
	
	def generateTable(Table t, Class c) {
		'''
		<table class="table table-striped" id="«t.elementID»">
			<thead>
				<tr>
					«FOR Column co : t.columns»
					<th>«co.label»</th>
					«ENDFOR»
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr data-ng-repeat="«t.association.navigableEnd.type.name.toLowerCase» in «t.association.navigableEnd.type.name.toLowerCase»s">
					«FOR Column co : t.columns»
					<td> {{ «t.association.navigableEnd.type.name.toLowerCase».«co.property.name» }} </td>
					«ENDFOR»
					<td>
			 		«FOR Link li : t.link»
			 			«generateLink(li)»
			 		«ENDFOR»
			 		</td>
				</tr>
			</tbody>
		</table>
		'''
	}
	
	def generateErrorSpanTags(PropertyElement pe, View v) {
		'''
		<span class="«v.name.replace(' ','')»Span" style="color:red"
		data-ng-show="«v.name.replace(' ','')»Form.«pe.label.replace(' ','').toLowerCase».$dirty &&
		«v.name.replace(' ','')»Form.«pe.label.replace(' ','').toLowerCase».$invalid">

		«IF pe.property.lowerBound == 1 && pe.property.upperBound == 1»
 		<span data-ng-show="«v.name.replace(' ','')»Form.«pe.label.replace(' ','').toLowerCase».$error.required">
 		Input is mandatory.
 		</span>
		«ENDIF»
		
		«IF (pe instanceof Text && (pe as Text).format != null) || (pe instanceof DateTimePicker && (pe as DateTimePicker).format != null) »
		<span data-ng-show="«v.name.replace(' ','')»Form.«pe.label.replace(' ','').toLowerCase».$error.pattern">
		Input doesn't match expected pattern.
		</span>
		«ENDIF»
		</span>
		'''
	}
	
	def generateCondition(Condition c, Class class_) {
		'''
		«IF c instanceof CompositeCondition»
			«var comp = c as CompositeCondition»
			data-ng-«generateDataNgType(c.type.getName)» = "«FOR temp : comp.composedConditions SEPARATOR generateConditionCharacter(comp.compositionType.getName)»
			«var vc = temp as ComparisonCondition»
			new«class_.name.toLowerCase».«vc.property.label.name» «generateConditionCharacter(vc.comparisonType.getName.name)» '«vc.comparisonValue»'
			«ENDFOR»"
		«ELSEIF c instanceof ComparisonCondition»
			«var cc = c as ComparisonCondition»
			data-ng-«generateDataNgType(cc.type.getName)» = "new«class_.name.toLowerCase».«cc.property.label.name» «generateConditionCharacter(cc.comparisonType.getName.name)» '«cc.comparisonValue»'"
		«ENDIF»
		'''
	}
	
	def generateDataNgType(String t) {
		switch(t.toLowerCase) {
			case "disable" : "disabled"
			case "enable" : "enabled"
			case "hide" : "hide"
			case "show" : "show"
			default: "ERROR"
		}
	}
	
	def generateConditionCharacter(String t) {
		switch(t.toLowerCase) {
			case "greater" : " > "
			case "equal" : " == "
			case "less" : " < "
			case "or" : " || "
			case "and" : " && "
			default: "ERROR"
		}
	}
	
	def generateAddButton(Link link) {
		'''
		«IF link.targetView instanceof CreateView»
			<button value="«link.targetView.name.replaceAll("\\W", "")»" class="btn btn-primary btn-sm">Add</button>
		«ENDIF»
		'''
	}
	
	def generateSaveButton(ClassOperationView view, ViewModel model) {
		'''
		«IF (view instanceof CreateView || view instanceof UpdateView) && !view.startView»
			<button value="«getStartViewNameOfWelcomeGroup(model).replaceAll("\\W", "")»" class="btn btn-primary btn-sm" data-ng-disabled="«view.name.replaceAll("\\W", "")»Form.$invalid" data-ng-click="save«view.class_.name»()">
			  Save
			</button>
		«ENDIF»
		'''
	}
	
	def generateModalButton(ClassOperationView view) {
		'''
		«IF view instanceof ReadView»
			<button class="btn btn-default" data-dismiss="modal">Close</button>
		«ELSEIF view instanceof DeleteView»
			<button class="btn btn-default" data-dismiss="modal" data-ng-click="delete«view.class_.name»(«view.class_.name.toLowerCase».id)">Delete</button>
			<button class="btn btn-default" data-dismiss="modal">Cancel</button>
		«ENDIF»
		'''
	}
	
	def generateLink(Link link) {
		'''
		«IF link.targetView instanceof ReadView»
			<a href="" data-toggle="modal" data-target="#modal«link.targetView.name.replaceAll("\\W", "")»" data-ng-click="get«link.targetView.class_.name»(«link.targetView.class_.name.toLowerCase».id)">show</a>
		«ELSEIF link.targetView instanceof DeleteView»
			<a href="" data-toggle="modal" data-target="#modal«link.targetView.name.replaceAll("\\W", "")»" data-ng-click="get«link.targetView.class_.name»(«link.targetView.class_.name.toLowerCase».id)">delete</a>
		«ELSEIF link.targetView instanceof UpdateView»
			<a href="" data-ng-click="navigation«link.targetView.header»('UpdateProfessor'); update«link.targetView.class_.name»(«link.targetView.class_.name.toLowerCase».id)">udpate</a>
		«ENDIF»
		'''
	}
	
	def getStartViewNameOfWelcomeGroup(ViewModel model) {
		return model.viewGroups.filter(vg | vg.isWelcomeViewGroup).get(0).views.filter(v | v.isStartView).get(0).name
	}

	def getWelcomeGroup(ViewModel model) {
		for(group : model.viewGroups)
			if(group.isWelcomeViewGroup)
				return group.name.replaceAll("\\W", "")
	}
	
	def getName(String st){
		return st.toLowerCase.replaceAll("\\W", "")
	}

}