package at.ac.tuwien.big.views.gen

import java.io.File
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.ac.tuwien.big.views.DomainModel
import at.ac.tuwien.big.views.Class
import at.ac.tuwien.big.views.DomainModelElement

class Domain2AngularJSGenerator implements IGenerator {
	
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		if (resource.contents.get(0) instanceof DomainModel) {
			var domainModel = resource.contents.get(0) as DomainModel
			val angularJSFileName = new File(getFirstClassName(domainModel).toLowerCase+".js");
			fsa.generateFile(
				angularJSFileName.toString,
				'''
				«generateModule(domainModel)»
				«generateService(domainModel)»
				«generateController(domainModel)»'''
			)
		}
	}
	
	def generateModule(DomainModel model) {
		'''var module = angular.module('«getFirstClassName(model)»App', []);'''
	}
	
	def generateService(DomainModel model) { '''
		module.service('«getFirstClassName(model)»Service', function () {
			«FOR DomainModelElement d : model.domainModelElements»
				«IF d instanceof Class»
					«generateClassService(d)»
				«ENDIF»
			«ENDFOR»
		});'''
	}
	
	def generateClassService(Class c) {
		'''
		var «c.name.toLowerCase»s = [];
		var «c.name.toLowerCase»id = 0;

		this.save«c.name»Service = function («c.name.toLowerCase») {
			if («c.name.toLowerCase».id == null) {
				«c.name.toLowerCase».id = «c.name.toLowerCase»id++;
				«c.name.toLowerCase»s.push(«c.name.toLowerCase»);
			} else {
				for (i in «c.name.toLowerCase»s) {
					if («c.name.toLowerCase»s[i].id == «c.name.toLowerCase».id) {
					   	«c.name.toLowerCase»s[i] = «c.name.toLowerCase»;
					}
				}
			}
		}

		this.get«c.name»Service = function (id) {
			for (i in «c.name.toLowerCase»s) {
				if («c.name.toLowerCase»s[i].id == id) {
					return «c.name.toLowerCase»s[i];
				}   
			}
		}

		this.delete«c.name»Service = function (id) {
			for (i in «c.name.toLowerCase»s) {
				if («c.name.toLowerCase»s[i].id == id) {
					«c.name.toLowerCase»s.splice(i, 1);
				}
			}
		}

		this.list«c.name»Service = function () {
			return «c.name.toLowerCase»s;
		}
		'''
	}
	
	def generateController(DomainModel model) {	'''
		module.controller('«getFirstClassName(model)»Controller', function ($scope, «getFirstClassName(model)»Service) {

«««			//add controllers here
			«FOR DomainModelElement d : model.domainModelElements»
				«IF d instanceof Class»
					«generateClassController(d, getFirstClassName(model) + "Service")»
				«ENDIF»
			«ENDFOR»
		});'''
	}
	
	def generateClassController(Class c, String serviceName) {
		'''
		$scope.«c.name.toLowerCase»s = «serviceName».list«c.name»Service();
			
		$scope.save«c.name» = function () {
			«serviceName».save«c.name»Service($scope.new«c.name.toLowerCase»);
			$scope.new«c.name.toLowerCase» = {};
		}
	
		$scope.delete«c.name» = function (id) {
			«serviceName».delete«c.name»Service(id);
		}
	
		$scope.update«c.name» = function (id) {
			$scope.new«c.name.toLowerCase» = angular.copy(«serviceName».get«c.name»Service(id));
		}
	
		$scope.get«c.name» = function (id) {
			$scope.«c.name.toLowerCase» = angular.copy(«serviceName».get«c.name»Service(id));
		}
				
		$scope.navigation«c.name» = function (targetView) {
			$(".container").hide(); 
			var view = angular.element('#'+targetView);
			view.show();
		}
		'''
	}
	
	def getFirstClassName(DomainModel model) {
		var classname = model.domainModelElements.filter(Class).get(0)
		return classname.name.toLowerCase.replaceAll("\\W", "");
	}	
}